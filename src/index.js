import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import './reset.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
const setFirstStorage = (key) =>{
    if (!localStorage.getItem(key)){
        localStorage.setItem(key, JSON.stringify([]))
    }
}
setFirstStorage("wishListId")
setFirstStorage("basketListId")

root.render(
    <App />
);

reportWebVitals();
