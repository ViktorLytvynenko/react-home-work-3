import {useState, useEffect} from "react";
import axios from "axios";
import {BrowserRouter, Routes, Route} from "react-router-dom"

import './App.css';

import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import BooksMain from "./components/BooksMain/BooksMain";
import Basket from "./components/Basket/Basket";
import WishList from "./components/WishList/WishList";

const App = () => {
    const [state, setState] = useState(
        {
            openBuyModal: false,
            openWishListModal: false,
            books: [],
            basket: localStorage.getItem('basket') ? parseInt(localStorage.getItem('basket')) : 0,
            wishList: localStorage.getItem('wishList') ? parseInt(localStorage.getItem('wishList')) : 0,
            arrWishList: JSON.parse(localStorage.getItem("wishListId")),
            arrBasketList: JSON.parse(localStorage.getItem("basketListId"))
        }
    )

    useEffect(() => {
        setState(prevState => ({
            ...prevState,
            basket: localStorage.getItem('basket') ? parseInt(localStorage.getItem('basket')) : 0,
            wishList: localStorage.getItem('wishList') ? parseInt(localStorage.getItem('wishList')) : 0
        }))
        axios.get("/books.json").then(res => {
            setState(prevState => ({
                ...prevState,
                books: [
                    ...res.data
                ]
            }))
        }).catch(error => {
            console.error('Ошибка загрузки книг', error);
        });
    }, [])
    const confirmOrder = () => {
        const idCandidate = localStorage.getItem('basketCandidate');
        localStorage.setItem('basket', state.basket + 1);
        setState({
            ...state,
            openBuyModal: false,
            basket: state.basket + 1,
            arrBasketList: [
                ...state.arrBasketList,
                idCandidate
            ],

        })
        const basketList = JSON.parse(localStorage.getItem("basketListId"))
        localStorage.setItem("basketListId", JSON.stringify([...basketList, idCandidate]))
    }
    const confirmWishList = () => {
        localStorage.setItem('wishList', state.wishList + 1);
        const idCandidate = localStorage.getItem('wishCandidate');

        setState({
            ...state,
            openWishListModal: false,
            wishList: state.wishList + 1,
            arrWishList: [
                ...state.arrWishList,
                idCandidate
            ]
        })
        const wishList = JSON.parse(localStorage.getItem('wishListId'))
        localStorage.setItem("wishListId", JSON.stringify([
            ...wishList,
            idCandidate
        ]))
    }
    const removeWishItem = (idCandidate) => {
        localStorage.setItem('wishList', (state.wishList - 1).toString());
        let newWishList = state.arrWishList.filter(id => {
            if (id !== idCandidate) {
                return id
            }
            return null
        })
        setState({
            ...state,
            openWishListModal: false,
            wishList: state.wishList - 1,
            arrWishList: [
                ...newWishList
            ]
        })
        const wishList = JSON.parse(localStorage.getItem('wishListId'))
        localStorage.setItem("wishListId", JSON.stringify([
            ...wishList.filter(id => id !== idCandidate)
        ]))
    }
    const handleClickToBuy = () => {
        setState({
            ...state,
            openBuyModal: state.openBuyModal === false
        })
    }
    const handleClickWishList = () => {
        setState({
            ...state,
            openWishListModal: state.openWishListModal === false
        })
    }
    const texts = {
        headerFirst: "Увага!",
        headerSecond: "Увага!",
        bodyFirst: "Зверніть увагу, що товар поверненню не підлягає",
        bodySecond: "Ви впевнені, що хочете додати даний товар до обраних?"
    }
    return (
        <BrowserRouter>
            <Header basket={state.basket} wishList={state.wishList}/>
            <main className="main">
                <div className={"container"}>
                    <Routes>
                        <Route path="/"
                               element={
                                   <BooksMain
                                       books={state.books}
                                       handleClickToBuy={handleClickToBuy}
                                       handleClickWishList={handleClickWishList}
                                       handleRemoveWish={removeWishItem}
                                       arrWishList={state.arrWishList}
                                       arrBasketList={state.arrBasketList}
                                   />
                               }
                        />
                        <Route path="/basket"
                               element={
                                   <Basket
                                       books={state.books}
                                       arrBasketList={state.arrBasketList}
                                       arrWishList={state.arrWishList}
                                       handleClickToBuy={handleClickToBuy}
                                       handleClickWishList={handleClickWishList}
                                       handleRemoveWish={removeWishItem}
                                   />

                               }
                        />
                        <Route path="/wishlist"
                               element={
                                   <WishList
                                       handleClickToBuy={handleClickToBuy}
                                       arrBasketList={state.arrBasketList}
                                       books={state.books}
                                       arrWishList={state.arrWishList}
                                       handleRemoveWish={removeWishItem}
                                   />
                               }
                        />
                    </Routes>
                </div>
            </main>
            <Footer/>
            {state.openBuyModal === true ?
                <Modal handleClick={handleClickToBuy} confirm={confirmOrder} text1={texts.headerFirst}
                       text2={texts.bodyFirst} theme="danger" isClosed={true}/> : null}
            {state.openWishListModal === true ?
                <Modal handleClick={handleClickWishList} confirm={confirmWishList} text1={texts.headerSecond}
                       text2={texts.bodySecond} theme="primary" isClosed={true}/> : null}
        </BrowserRouter>
    )
    
}

export default App;
