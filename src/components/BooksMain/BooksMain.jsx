import style from "./BooksMain.module.scss";
import BookItem from "./Book-item/BookItem";
import PropTypes from "prop-types";

const BooksMain = (props) => {
    const BooksCollection = props.books.map((book, index) =>{
        return <BookItem
            key={index}
            arrWishList={props.arrWishList}
            id={book.id}
            img={book.img}
            author={book.author}
            name={book.name}
            price={book.price}
            handleClickToBuy={props.handleClickToBuy}
            handleClickWishList={props.handleClickWishList}
            handleRemoveWish = {props.handleRemoveWish}
        />
    })
        return (
        <div className={style.main_container}>
            {BooksCollection}
        </div>
    )
}
BooksMain.propTypes = {
    books: PropTypes.array.isRequired,
    arrWishList: PropTypes.array,
    handleClickToBuy: PropTypes.func.isRequired,
    handleClickWishList: PropTypes.func.isRequired,
    handleRemoveWish: PropTypes.func.isRequired
};

BooksMain.defaultProps = {
    arrWishList: [],
};

export default BooksMain